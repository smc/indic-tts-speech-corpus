## Source

The original source of data in the directory IndicTTS is: https://www.iitm.ac.in/donlab/tts/index.php

## Copyright Notice

COPYRIGHT   2016   TTS   Consortium,   TDIL,   Meityrepresented by Hema A Murthy & S Umesh, DEPARTMENT OFComputer   Science   and   Engineering   and   ElectricalEngineering, IIT Madras. ALL RIGHTS RESERVED

## License

License details can be found in `license.pdf` file in this directory

## Content

Sentences in Malayalam language spoken by native Malayalam speakers. Transcription of each speech waveform in Malayalam script is also available.


